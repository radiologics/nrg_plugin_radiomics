# Radiomics Datatype Plugin Changelog

This plugin adds support for the Radiomics Image Assessor datatype. 

## Radiomics Plugin Version 1.2.0
Released 2024-10-25

* [INTERNAL-1872](https://radiologics.atlassian.net/browse/INTERNAL-1872) Update dependencies for compatibility with XNAT 1.9.0 and update gradle version


## Radiomics Plugin Version 1.1.2
Released 2023-04-26
